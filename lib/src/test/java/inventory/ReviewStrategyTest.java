package inventory;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ReviewStrategyTest {

    @Test void thereAreExactlyTwoReviewStrategies() {
        assertThat(ReviewStrategy.values()).hasSize(2);
    }

    @Test void oneOfTheReviewStrategiesIsNamedCONTINUOUS() {
        assertThat(ReviewStrategy.CONTINUOUS).isNotNull();
    }

    @Test void oneOfTheReviewStrategiesIsNamedPERIODIC() {
        assertThat(ReviewStrategy.PERIODIC).isNotNull();
    }
}
