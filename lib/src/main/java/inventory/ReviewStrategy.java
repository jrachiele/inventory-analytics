package inventory;

/**
 * There are two main inventory review strategies commonly adopted: continuous and periodic (Rossi 2021, p.25).
 */
public enum ReviewStrategy {

    /**
     * Continuously records receipt and disbursement for every item.
     */
    CONTINUOUS,

    /**
     * A physical count (stock take) at the end of each period (e.g., weekly or monthly).
     */
    PERIODIC
}
